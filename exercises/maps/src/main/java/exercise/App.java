package exercise;


import java.util.HashMap;
import java.util.Map;

class App {

    public static Map<String, Integer> getWordCount(String sentence) {
        Map<String, Integer> wordsCount = new HashMap<>();
        String[] words = sentence.split(" ");
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (!word.isEmpty()) {
                Integer count = wordsCount.getOrDefault(word, 0);
                wordsCount.put(word, count + 1);
            }
        }
        return wordsCount;
    }

    public static String toString(Map<String, Integer> wordCounts) {

        StringBuilder mapAsString = new StringBuilder("{");
        if (wordCounts.isEmpty()) {
            mapAsString.append("}");
        } else {

            for (String key : wordCounts.keySet()) {

                mapAsString.append("\n" + "  ").append(key).append(":").append(" ").append(wordCounts.get(key));
            }
            mapAsString.append("\n" + "}");
        }
        return mapAsString.toString();
    }
}
// BEGIN

//END
