package exercise;

class App {

    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        if ((a + b)<= c || (a + c) <= b || (b + c) <= a) {
            return "Треугольник не существует";
        } else if (a != b && a != c && b != c) {
            return "Разносторонний";
        } else if (a == b && a != c || a == c && a != b || b == c && b != a) {
            return "Равнобедренный";
        } else {
            return "Равносторонний";
        }
    }
        public static int getFinalGrade ( int exam, int project){
            if (exam > 90 || project > 10) {
                return 100;
            } else if (exam > 75 && project >= 5) {
                return 90;
            } else if (exam > 50 && project >= 2) {
                return 75;
            } else {
                return 0;
            }
        }

    // END
}
