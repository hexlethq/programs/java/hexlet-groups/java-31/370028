package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class AppTest {
    @Test
    void testGetTypeOfTriangle1() {
        String result = App.getTypeOfTriangle(1, 2, 7);
        assertThat(result).isEqualTo("Треугольник не существует");
    }

    @Test
    void testGetTypeOfTriangle2() {
        String result = App.getTypeOfTriangle(5, 6, 7);
        assertThat(result).isEqualTo("Разносторонний");
    }

    @Test
    void testGetTypeOfTriangle3() {
        String result = App.getTypeOfTriangle(5, 6, 5);
        assertThat(result).isEqualTo("Равнобедренный");
    }

    @Test
    void testGetTypeOfTriangle4() {
        String result = App.getTypeOfTriangle(5, 5, 5);
        assertThat(result).isEqualTo("Равносторонний");
    }

    @Test
    void testGetTypeOfTriangle5() {
        String result = App.getTypeOfTriangle(1, -2, 7);
        assertThat(result).isEqualTo("Треугольник не существует");
    }

    @Test
    void testFinalGrade() {

        int result = App.getFinalGrade(100, 12); // 100
        assertThat(result).isEqualTo(100);
        int result2 = App.getFinalGrade(99, 0); // 100
        assertThat(result2).isEqualTo(100);
        int result3 = App.getFinalGrade(10, 15); // 100
        assertThat(result3).isEqualTo(100);
        int result4 = App.getFinalGrade(85, 5); // 90
        assertThat(result4).isEqualTo(90);
        int result5 = App.getFinalGrade(55, 3); // 75
        assertThat(result5).isEqualTo(75);
        int result6 = App.getFinalGrade(55, 0); // 0
        assertThat(result6).isEqualTo(0);

    }

    // BEGIN
    
    // END
}
