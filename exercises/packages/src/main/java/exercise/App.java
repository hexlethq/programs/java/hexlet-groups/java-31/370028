// BEGIN
package exercise;

import exercise.geometry.point.Point;
import exercise.geometry.Segment;

import static exercise.geometry.Segment.getBeginPoint;
import static exercise.geometry.Segment.getEndPoint;
import static exercise.geometry.Segment.makeSegment;
import static exercise.geometry.point.Point.getX;
import static exercise.geometry.point.Point.getY;
import static exercise.geometry.point.Point.makePoint;

public class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        double midBeginPointX = (getX(getBeginPoint(segment)) + getX(getEndPoint(segment))) / 2;
        double midEndPointY = (getY(getBeginPoint(segment)) + getY(getEndPoint(segment))) / 2;
        double[] midPoint = new double[]{midBeginPointX, midEndPointY};
        return midPoint;
    }

    public static double[][] reverse(double[][] segment) {
        double[] endPoint = getEndPoint(segment);
        double[] beginPoint = getBeginPoint(segment);
        double[] reversedBeginPoint = makePoint(endPoint[0], endPoint[1]);
        double[] reversedEndPoint = makePoint(beginPoint[0], beginPoint[1]);
        double[][] reversedSegment;
        reversedSegment = makeSegment(reversedBeginPoint, reversedEndPoint);
        return reversedSegment;
    }

    public static boolean isBelongToOneQuadrant(double[][] segment) {
        double beginPointX = getX(getBeginPoint(segment));
        double endPointX = getX(getEndPoint(segment));
        double beginPointY = getY(getBeginPoint(segment));
        double endPointY = getY(getEndPoint(segment));
        if (beginPointX > 0 && endPointX > 0 && beginPointY > 0 && endPointY > 0) {
            return true;
        }
        if (beginPointX > 0 && endPointX > 0 && beginPointY < 0 && endPointY < 0) {
            return true;
        }
        if (beginPointX < 0 && endPointX < 0 && beginPointY < 0 && endPointY < 0) {
            return true;
        }
        if (beginPointX < 0 && endPointX < 0 && beginPointY > 0 && endPointY > 0) {
            return true;
        } else {
            return false;
        }
    }
}
// END
