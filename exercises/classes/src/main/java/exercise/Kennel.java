package exercise;

class Kennel {


    private static String[][] allPuppies = new String[][]{};


    // BEGIN
    public static void addPuppy(String[] puppy) {
        int size = allPuppies.length;
        String[][] copy = new String[size + 1][2];
        for (int i = 0; i < size; i++) {
            copy[i] = allPuppies[i];
        }
        copy[size] = puppy;
        allPuppies = copy;

    }

    public static int getPuppyCount() {
        return allPuppies.length;
    }


    public static boolean isContainPuppy(String name) {
        int size = allPuppies.length;
        for (int i = 0; i < size; i++) {
            String puppyName = allPuppies[i][0];
            if (puppyName.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public static void addSomePuppies(String[][] puppies) {
        int size = allPuppies.length;
        int sizePuppies = puppies.length;
        String[][] copy = new String[size + sizePuppies][2];
        for (int i = 0; i < size; i++) {
            copy[i] = allPuppies[i];
        }
        for (int i = size, k = 0; i < size + sizePuppies; i++, k++) {
            copy[i] = puppies[k];
        }
        allPuppies = copy;
    }


    public static String[][] getAllPuppies() {

        return allPuppies;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] puppiesNames = new String[]{};
        int size = allPuppies.length;
        for (int i = 0; i < size; i++) {
            String puppyName = allPuppies[i][0];
            String puppyBreed = allPuppies[i][1];
            if (puppyBreed.equals(breed)) {
                String[] copy = new String[puppiesNames.length + 1];
                for (int j = 0; j < puppiesNames.length; j++) {
                    copy[j] = puppiesNames[j];
                }
                copy[puppiesNames.length] = puppyName;
                puppiesNames = copy;

            }
        }
        return puppiesNames;
    }

    public static void resetKennel() {
        allPuppies = new String[][]{};
    }


    public static boolean removePuppy(String name) {
        int size = allPuppies.length;
        int sizeCopy = allPuppies.length - 1;

        for (int i = 0; i < size; i++) {
            String puppyName = allPuppies[i][0];
            if (puppyName.equals(name)) {
                String[][] copy = new String[sizeCopy][2];
                int k = 0;
                for (int j = 0; j < size; j++) {
                    if (j != i) {
                        copy[k] = allPuppies[j];
                        k++;

                    }

                }
                allPuppies = copy;
                return true;

            }
        }
        return false;


    }


// END
}