package exercise;

class App {
    // BEGIN
    public static int [] reverse(int [] array) {
        int[] reverseNumbers = new int[array.length];
        int arraySize = reverseNumbers.length - 1;
        int index = 0;
        for (int i = arraySize; i >= 0; i--) {
            reverseNumbers[index] = array[i];
            index++;
        }
        return reverseNumbers;
    }

    public static int  mult (int [] array) {

        int arraySize = array.length;
        int pNumbers = 1;
        for(int i = 0; i < arraySize; i++) {
            pNumbers *= array[i];
        }
        return pNumbers;
    }


public static int [] flattenMatrix(int [][] arrays) {
    int arraysSize = 0;
    if (arrays.length > 0) {
        arraysSize = arrays[0].length;
    }
    int[] oneArrays = new int[arrays.length * arraysSize];
    int k = 0;
    for (int i = 0; i < arrays.length; i++) {
        for (int j = 0; j < arrays[i].length; j++) {
            oneArrays[k] = arrays[i][j];
            k++;
        }
    }
    return oneArrays;
}

    // END
}
