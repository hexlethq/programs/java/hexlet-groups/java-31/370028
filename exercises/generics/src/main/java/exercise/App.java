package exercise;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
class App {

    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> dictionary) {
        List<Map<String, String>> resultBooks = new ArrayList<>();
        for (Map<String, String> currentBook : books) {
            if (isSuitable(dictionary, currentBook)) {
                resultBooks.add(currentBook);
            }
        }
        return resultBooks;
    }

    private static boolean isSuitable(Map<String, String> dictionary, Map<String, String> currentBook) {
        Set<String> filterKeys = dictionary.keySet();
        for (String key : filterKeys) {
            String filterValue = dictionary.get(key);
            String currentBookValue = currentBook.get(key);
            if (!filterValue.equals(currentBookValue)) {
                return false;
            }
        }
        return true;
    }
}
//END
