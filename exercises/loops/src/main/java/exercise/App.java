package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String sentence) {


        sentence = sentence.trim();
        int length = sentence.length();
        char symbol;
        char prevSymbol;
        String result = "" + Character.toUpperCase(sentence.charAt(0));
        for(int i = 1; i < length; i++) {
            symbol = sentence.charAt(i);
            prevSymbol = sentence.charAt(i - 1);
            if (prevSymbol == ' ' && symbol != ' ') {
               result += Character.toUpperCase(symbol);
            }
        }
        return result;
    }

    // END
}
