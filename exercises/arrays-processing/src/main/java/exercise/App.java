package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {
        if (arr.length == 0) {
            return -1;
        }
        int maxNegative = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && arr[i] > maxNegative) {
                maxNegative = arr[i];
                index = i;
            }
        }
        return index;
    }

    public static int[] getElementsLessAverage(int[] arr) {
        int[] lessAverage = new int[arr.length];
        int arrSize = lessAverage.length;
        int index = 0;
        double average = 0;
        if (arr.length > 0) {
            double sum = 0;
            for (int i = 0; i < arr.length; i++) {
                sum += arr[i];
            }
            average = sum / arr.length;
        } else {
            return lessAverage;
        }
        for (int i = 0; i < arrSize; i++) {
            int value = arr[i];
            if (value < average) {
                lessAverage[index] = arr[i];
                index++;
            }
        }

        int[] copyOfLessAverage = Arrays.copyOf(lessAverage, index);
        return copyOfLessAverage;
    }


    public static int getSumBeforeMinAndMax(int[] arr) {
        int arrSize = arr.length;
        int sum = 0;
        int max = arr[0];
        int min = arr[0];
        int indexMax = -1;
        int indexMin = -1;
        for (int i = 0; i < arrSize; i++) {
            if (arr[i] > max) {
                max = arr[i];
                indexMax = i;
            }
            if (arr[i] < min) {
                min = arr[i];
                indexMin = i;
            }
        }
        int lastIndex = Math.max(indexMax, indexMin);
        int firstIndex = Math.min(indexMax, indexMin);

        int[] result = Arrays.copyOfRange(arr, firstIndex + 1, lastIndex);
        if (result.length == 0) {
            return 0;
        }
        for (int i = 0; i < result.length; i++) {
            sum += result[i];

        }
        return sum;
    }


}
