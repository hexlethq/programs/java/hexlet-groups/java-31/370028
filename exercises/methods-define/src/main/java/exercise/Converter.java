package exercise;

class Converter {
    // BEGIN
    public static int convert(int number , String str) {
        if ("b".equals(str)) {
            return number * 1024;
        } else if("Kb".equals(str)) {
            return number / 1024;
        } else {
            return 0;
        }
    }
    public static void main(String[] args) {
        var text = "10 Kb = " + convert(10, "b") + " b";
        System.out.println(text);
    }
    // END
}
