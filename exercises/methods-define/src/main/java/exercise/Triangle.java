package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double oneSide , double twoSide , double angle ) {
        return ((oneSide * twoSide) / 2 * Math.sin(angle * Math.PI / 180));
    }
    public static void main(String[] args) {
        double a = 4;
        double b = 5;
        double c = 45;
        System.out.println(getSquare(a,b,c));
    }
    // END
}
