package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] array) {
        int arraySize = array.length;
        if (arraySize == 0) {
            return array;
        }
        int result;
        for (int i = 0; i < arraySize; i++) {
            for (int k = 0; k < arraySize - 1; k++) {
                if (array[k] > array[k + 1]) {
                    result = array[k];
                    array[k] = array[k + 1];
                    array[k + 1] = result;
                }
            }
        }
        return array;
    }


    public static int[] sort2(int[] array) {
        int arraySize = array.length;
        if (arraySize == 0) {
            return array;
        }
        int result;
        for (int i = 0; i < arraySize; i++) {
            int min = array[i];
            int indexMin = i;
            for (int j = i + 1; j < arraySize; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexMin = j;
                }
            }
            result = array[i];
            array[i] = min;
            array[indexMin] = result;


        }
        return array;
    }
    // END
}
