package exercise;

class App {
    public static void numbers() {
        // BEGIN
        System.out.println((8 / 2) + (100 % 3));
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        String strWorks = " works";
        String strOn = " on";
        String strJVM = " JVM";
        System.out.println(language + strWorks + strOn + strJVM);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println((int) soldiersCount + " " + name);
        // END
    }
}
