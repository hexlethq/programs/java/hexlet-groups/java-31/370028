package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int xCoordinate, int yCoordinate) {
        int[] point = new int[2];
        point[0] = xCoordinate;
        point[1] = yCoordinate;
        return point;
    }


    public static int getX(int[] point) {
        int x = point[0];
        return x;
    }


    public static int getY(int[] point) {
        int y = point[1];
        return y;
    }


    public static String pointToString(int[] point) {
        return "(" + getX(point) + ", " + getY(point) + ")";
    }


    public static int getQuadrant(int[] point) {
        int x = getX(point);
        int y = getY(point);
        if (x == 0 || y == 0) {
            return 0;
        } else if (x > 0 && y > 0) {
            return 1;
        } else if (x > 0 && y < 0) {
            return 4;
        } else if (x < 0 && y > 0) {
            return 2;
        } else {
            return 3;
        }
    }


    public static int[] getSymmetricalPointByX(int[] point) {
        return makePoint(getX(point), -getY(point));
    }


    public static double calculateDistance(int[] pointA, int[] pointB) {

        double distance =  Math.sqrt(Math.pow(getX(pointA) - getX(pointB), 2)
                + Math.pow(getY(pointA) - getY(pointB), 2));
        return distance;
    }
    // END
}
