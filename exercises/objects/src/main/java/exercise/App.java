package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

class App {
    // BEGIN
    public static String buildList(String[] array) {
        int arraySize = array.length;
        if (arraySize == 0) {
            return "";
        }
        String prefix = "<ul>\n";
        String suffix = "</ul>";
        StringBuilder items = new StringBuilder(prefix);
        for (int i = 0; i < arraySize; i++) {
            if (array[i] != null) {
                items.append("  <li>")
                        .append(array[i])
                        .append("</li>\n");
            }
        }
        items.append(suffix);
        String result = items.toString();
        return result;
    }

    public static String getUsersByYear(String[][] arrays, int year) {
        int arraysSize = arrays.length;
        String[] resultArray = new String[arraysSize];
        int indexCorrectAnswer = 0;
        for (int i = 0; i < arraysSize; i++) {
            DateTimeFormatter userFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String elem = arrays[i][1];
            LocalDate specifiedDateForUser = LocalDate.parse(elem, userFormatter);
            int currentYear = specifiedDateForUser.getYear();
            if (currentYear == year) {
                resultArray[indexCorrectAnswer] = arrays[i][0];
                indexCorrectAnswer++;
            }
        }
        if (indexCorrectAnswer == 0) {
            return "";
        }
        return buildList(resultArray);
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        int arraysSize = users.length;
        String youngestUser = "";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy", java.util.Locale.ENGLISH);
        LocalDate beforeSpecifiedDate = LocalDate.parse(date, formatter);
        LocalDate youngestUserDate = LocalDate.MIN;
        for (int i = 0; i < arraysSize; i++) {
            DateTimeFormatter userFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String elem = users[i][1];
            LocalDate specifiedDateForUser = LocalDate.parse(elem, userFormatter);

            if (specifiedDateForUser.isBefore(beforeSpecifiedDate) && specifiedDateForUser.isAfter(youngestUserDate)) {
                youngestUserDate = specifiedDateForUser;
                youngestUser = users[i][0];
            }

        }
        return youngestUser;
    }


    // END
}

