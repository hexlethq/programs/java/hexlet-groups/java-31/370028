package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
class App {
    public static boolean scrabble(String symbols, String word) {
        List<Character> list = new ArrayList<>();
        for (int i = 0; i < symbols.length(); i++) {
            list.add(symbols.charAt(i));
        }
        String word2 = word.toLowerCase();
        for (int j = 0; j < word2.length(); j++) {
            boolean result = list.remove(Character.valueOf(word2.charAt(j)));
            if (!result) {
                return false;
            }
        }
        return true;
    }
}
//END
